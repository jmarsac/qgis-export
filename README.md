# QGIS Export

# For QGIS we need:

        fill="param(fill) #FFF"
        stroke="param(outline) #000"
        stroke-width="param(outline-width) 1"
        Fill opacity: fill-opacity="param(fill-opacity)"
        stroke-opacity="param(outline-opacity)"

The extension can convert elements (or just a selection of elements) to the above formatting. The orginal style for that element is deleted.
